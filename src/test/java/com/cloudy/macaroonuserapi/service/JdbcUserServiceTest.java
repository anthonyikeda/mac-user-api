package com.cloudy.macaroonuserapi.service;

import com.cloudy.macaroonuserapi.domain.User;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
public class JdbcUserServiceTest {

    private Logger log = LoggerFactory.getLogger(JdbcUserServiceTest.class);

    @Autowired
    private UserService service;

    @Before
    public void setup() {

    }

    @Test
    public void testGetAllUsers() {
        log.debug("Loading all users...");
        List<User> user = service.getUsers()
                .orElseThrow(() -> new RuntimeException("Unable to get all users"));

        Assertions.assertThat(user).isNotNull();
    }

    @Test
    public void createUser() {
        String uuid = UUID.randomUUID().toString();
        service.createUser(String.format("user%s", uuid), "password", String.format("user%s@home.com", uuid));

        User user = service.getUser(String.format("user%s", uuid)).orElseThrow(() -> new RuntimeException("Didn't find user"));

        Assertions.assertThat(user).isNotNull();
    }

}
