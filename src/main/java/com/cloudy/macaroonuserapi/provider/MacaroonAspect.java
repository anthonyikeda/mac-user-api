package com.cloudy.macaroonuserapi.provider;

import com.cloudy.macaroonuserapi.domain.MacaroonUser;
import com.github.nitram509.jmacaroons.Macaroon;
import com.github.nitram509.jmacaroons.MacaroonsBuilder;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.security.Principal;

@Aspect
@Component
public class MacaroonAspect {

    private Logger log = LoggerFactory.getLogger(getClass());

    public Object verifyCaveats(ProceedingJoinPoint joinPoint) throws Throwable {

        log.debug("Validating Macaroon Caveats!");
        Object[] args = joinPoint.getArgs();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Object target = joinPoint.getTarget();
        Class targetClass = AopProxyUtils.ultimateTargetClass(target);
        Method method = targetClass.getDeclaredMethod(signature.getName(), signature.getParameterTypes());
        Annotation[] annotations = method.getDeclaredAnnotations();

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        MacaroonUser principal = (MacaroonUser) authentication.getPrincipal();
        String authToken = (String) principal.getPassword();

        log.info(authToken);
        Macaroon mac = MacaroonsBuilder.deserialize((String) authToken);

        for (Annotation ann : annotations) {
            if (ann instanceof MacaroonCaveats) {
                MacaroonCaveats macAnnotation = (MacaroonCaveats) ann;
                String[] caveats = macAnnotation.caveats();
                for (String caveat : caveats) {
                    log.info(caveat);
                }
            }
        }
        log.debug("Args are: {}", args);
        return joinPoint.proceed();
    }
}
