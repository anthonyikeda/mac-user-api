package com.cloudy.macaroonuserapi.provider;

import org.springframework.security.core.AuthenticationException;

public class MacaroonReadException extends AuthenticationException {
    public MacaroonReadException(String msg, Throwable t) {
        super(msg, t);
    }

    public MacaroonReadException(String msg) {
        super(msg);
    }
}
