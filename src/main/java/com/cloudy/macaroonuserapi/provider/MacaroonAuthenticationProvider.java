package com.cloudy.macaroonuserapi.provider;

import com.cloudy.macaroonuserapi.config.MacaroonConfig;
import com.cloudy.macaroonuserapi.domain.MacaroonUser;
import com.cloudy.macaroonuserapi.service.UserService;
import com.github.nitram509.jmacaroons.Macaroon;
import com.github.nitram509.jmacaroons.MacaroonsBuilder;
import com.github.nitram509.jmacaroons.MacaroonsVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class MacaroonAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private Logger log = LoggerFactory.getLogger(MacaroonAuthenticationProvider.class);

    private MacaroonConfig macaroonConfig;

    public MacaroonAuthenticationProvider(MacaroonConfig _config) {
        this.macaroonConfig = _config;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        log.debug("Performing additional authentication checks for {}", userDetails.getUsername());
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        log.debug("Retrieving user {} in provider", username);
        Object authToken = authentication.getCredentials();

        try {
            Macaroon macaroon = MacaroonsBuilder.deserialize((String) authToken);
            log.debug("Macaroons id: {}, Location: {}", macaroon.identifier, macaroon.location);

            MacaroonsVerifier verifier = new MacaroonsVerifier(macaroon);

            verifier.isValid(macaroonConfig.getSecret());

            MacaroonUser user = new MacaroonUser();
            user.setUsername(macaroon.identifier);
            user.setEmailAddress(macaroon.location);
            user.setPassword(macaroon.signature);

            // Since the macaroon is verified the user is considered "enabled"
            // The LoginServer will determine the field from the database. If the user is disabled, it will not issue
            // the macaroon.
            user.setEnabled(true);
            return user;
        } catch (Exception e) {
            log.error("Error reading macaroon!", e);
            throw new MacaroonReadException("Error reading macaroon!", e);
        }

    }
}
