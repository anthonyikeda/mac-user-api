package com.cloudy.macaroonuserapi.service;


import com.cloudy.macaroonuserapi.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    void createUser(String username, String password, String emailAddress);

    Optional<User> getUser(String username);

    Optional<List<User>> getUsers();

    void enableUser(String username);
}
