package com.cloudy.macaroonuserapi.service;

import com.cloudy.macaroonuserapi.domain.MacaroonUser;
import com.cloudy.macaroonuserapi.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;


@Service
public class JdbcUserService implements UserService {

    private Logger log = LoggerFactory.getLogger(JdbcUserService.class);

    final String INSERT_SQL = "insert into authentication(username, password, email_address) values(?, ?, ?)";

    final String GETUSER_SQL = "select id, email_address, enabled from authentication where username = ?";

    final String ALLUSERS_SQL = "select * from authentication";

    final String ENABLEUSER_SQL = "update authentication set enabled = ? where username = ?";

    private JdbcTemplate template;
    private PasswordEncoder encoder;

    @Autowired
    public JdbcUserService(JdbcTemplate _template) {
        this.template = _template;
        this.encoder = new BCryptPasswordEncoder(10);
    }

    @Override
    public void createUser(String username, String password, String emailAddress) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.template.update((Connection con) -> {
                PreparedStatement ps = con.prepareStatement(INSERT_SQL, new String[]{"id"});
                ps.setString(1, username);
                ps.setString(2, encoder.encode(password));
                ps.setString(3, emailAddress);
                return ps;
        }, keyHolder);

        log.debug("User saved with id {}", keyHolder.getKey());
    }

    @Override
    public Optional<User> getUser(String username) {
        User found = this.template.query((Connection con) -> {
                    PreparedStatement ps = con.prepareStatement(GETUSER_SQL);
                    ps.setString(1, username);
                    return ps;
                },
                (ResultSet rs) -> {
                    if(rs.next()) {
                        MacaroonUser user = new MacaroonUser();
                        user.setEmailAddress(rs.getString("email_address"));
                        user.setUsername(username);
                        user.setUserId(rs.getInt("id"));

                        return user;
                    }
                    return null;
                });
        return Optional.ofNullable(found);
    }

    @Override
    public Optional<List<User>> getUsers() {
        log.debug("Loading all users...");
        List<User> users = this.template.query(
                (Connection con) -> {
                    PreparedStatement pstmt = con.prepareStatement(ALLUSERS_SQL);
                    return pstmt;
                },
                (ResultSet rs, int rowNum) -> {
                    MacaroonUser user = new MacaroonUser();
                    user.setEmailAddress(rs.getString("email_address"));
                    user.setUserId(rs.getInt("id"));
                    user.setUsername(rs.getString("username"));

                    return user;
                });
        return Optional.ofNullable(users);
    }

    /**
     * Yes we can create a generic "flip enabled" method, but for now this will do.
     *
     * @param username Name of user to change enabled status to true
     */
    @Override
    public void enableUser(String username) {
        log.debug("Enabling user {}", username);
        this.template.execute((Connection con) -> {
            PreparedStatement ps = con.prepareStatement(ENABLEUSER_SQL);
            ps.setBoolean(1, true);
            ps.setString(2, username);
            return ps;
        });
    }

}
