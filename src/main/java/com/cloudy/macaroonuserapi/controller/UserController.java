package com.cloudy.macaroonuserapi.controller;

import com.cloudy.macaroonuserapi.domain.User;
import com.cloudy.macaroonuserapi.provider.MacaroonCaveats;
import com.cloudy.macaroonuserapi.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
public class UserController {

    private Logger log = LoggerFactory.getLogger(UserController.class);

    private UserService userService;

    public UserController(UserService _userService) {
        this.userService = _userService;
    }

    @RequestMapping(path = "/user", method=RequestMethod.POST)
    public ResponseEntity<String> createUser(@RequestParam("username") String username,
                                             @RequestParam("password") String password,
                                             @RequestParam("email") String emailAddress) {
        log.debug("Creating new user with username {}", username);
        this.userService.createUser(username, password, emailAddress);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    //annotation to define the caveats, AOP Before any method that implements the interface assert the caveats they expect.
    @RequestMapping(path = "/user", method = RequestMethod.GET, produces = {"application/json"})
    @MacaroonCaveats(caveats = { "name=ikeda", "time<{date}"})
    public ResponseEntity<List<User>> getUsers() {
        log.debug("Listing alls users...");
        List<User> users = this.userService.getUsers().orElse(Collections.EMPTY_LIST);

        log.debug("Found {} users", users.size());

        return ResponseEntity.ok(users);
    }

}
