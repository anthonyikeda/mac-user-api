package com.cloudy.macaroonuserapi.domain;

import org.springframework.security.core.userdetails.UserDetails;

public interface User extends UserDetails {

    String getUsername();

}
