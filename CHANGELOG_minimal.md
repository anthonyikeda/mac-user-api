# Git Changelog Macaroon User API

Changelog of Macaroon User API.

## user-api-0.0.1
### No issue

**Issue #1 trying to make sure the release plugin works. Seems to be having auth issues.**


[8597c1e046ac63b](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/8597c1e046ac63b) Anthony Ikeda *2018-07-01 19:08:40*

**Updated gitignore to ignore hsqldb database files**


[0abab701cc4b172](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/0abab701cc4b172) Anthony Ikeda *2018-07-01 19:04:11*

**Issue #1 Added in the gradle version plugin**

* Updated the logic to throw an AuthenticationException if the macaroon is damaged
* Ensured we use macaroon.isValid() rather than macaroon.assertIsValid()
* Cleaned up handling of the database for Unit Tests

[818e46ebf2ff9e0](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/818e46ebf2ff9e0) Anthony Ikeda *2018-07-01 19:00:03*

**Fixed the incorrect migration information in the README file.**


[0ef9dcd688305f9](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/0ef9dcd688305f9) Anthony Ikeda *2018-06-29 14:00:41*

**Updating the readme with some setup details and example calls**


[82f418e4e343d40](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/82f418e4e343d40) Anthony Ikeda *2018-06-10 20:32:26*

**Some explanation on the macaroon side of an "enabled" user.**


[20e14806addcdb1](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/20e14806addcdb1) Anthony Ikeda *2018-06-10 20:21:29*

**Secret now loaded from the MacaroonConfig!**


[1ac6ad6638a9256](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/1ac6ad6638a9256) Anthony Ikeda *2018-06-10 20:14:33*

**Removed commented code.**


[c1d1ef1a80364cd](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/c1d1ef1a80364cd) Anthony Ikeda *2018-06-10 20:04:18*

**Authentication with macaroon seems to be working, no results from the user service though**


[2adb24ec738f4f0](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/2adb24ec738f4f0) Anthony Ikeda *2018-06-10 20:02:32*

**Refactor all the user user management code and Macaroon validation to here. Login server will only take a basic auth**

* and then supply the macaroon utilising this API to manage the data.
* Ideally the Login server on startup has it&#39;s own valid macaroon at all times.

[8f77166c0fe4a70](https://github.com/tomasbjerre/git-changelog-gradle-plugin/commit/8f77166c0fe4a70) Anthony Ikeda *2018-06-10 00:44:42*


