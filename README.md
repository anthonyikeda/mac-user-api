# User API

## Overview

API to manage users that is secured using macaroons.

Aside from validating a macaroon in the `Authorization: macaroon {macaroon}` header, it's just a way 
to manage users for authentication and authorization purposes.

## Getting up and Running

The API relies on a PostgreSQL database.

Start a Postgresql instance on Docker:

```bash
$ docker run -e POSTGRES_USER=auth_user -e POSTGREW_PASSWORD=password -p 5432:5432 postgres:10.4
```

Connect and create the database:

```bash
$ psql -h localhost -U auth_user 
# create database authentication_db
# create database authentication_test_db
```

Run the liquibase database migration:

```bash
$ ./gradlew dev update
```

To view the SQL required to build the database run: (This does not create the tables!)
```bash
$ ./gradlew dev updateSQL
liquibase-plugin: Running the 'main' activity...
INFO 6/10/18, 1:30 PM: liquibase: Successfully acquired change log lock
INFO 6/10/18, 1:30 PM: liquibase: Reading from public.databasechangelog
INFO 6/10/18, 1:30 PM: liquibase: /Users/anthonyikeda/work/git_poc/macaroon-user-api/src/main/resources/database-change-log.yaml: /Users/anthonyikeda/work/git_poc/macaroon-user-api/src/main/resources/changelog/01-changelog-tables.yaml::1::aikeda: Table authentication created
INFO 6/10/18, 1:30 PM: liquibase: /Users/anthonyikeda/work/git_poc/macaroon-user-api/src/main/resources/database-change-log.yaml: /Users/anthonyikeda/work/git_poc/macaroon-user-api/src/main/resources/changelog/01-changelog-tables.yaml::1::aikeda: ChangeSet /Users/anthonyikeda/work/git_poc/macaroon-user-api/src/main/resources/changelog/01-changelog-tables.yaml::1::aikeda ran successfully in 5ms
INFO 6/10/18, 1:30 PM: liquibase: Successfully released change log lock
-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: /Users/anthonyikeda/work/git_poc/macaroon-user-api/src/main/resources/database-change-log.yaml
-- Ran at: 6/10/18, 1:30 PM
-- Against: auth_user@jdbc:postgresql://localhost:5432/authentication_db
-- Liquibase version: 3.4.2
-- *********************************************************************

-- Lock Database
UPDATE public.databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = '2600:1010:b062:a586:41a4:13c9:3242:c9c1%en0 (2600:1010:b062:a586:41a4:13c9:3242:c9c1%en0)', LOCKGRANTED = '2018-06-10 13:30:39.701' WHERE ID = 1 AND LOCKED = FALSE;

-- Changeset /Users/anthonyikeda/work/git_poc/macaroon-user-api/src/main/resources/changelog/01-changelog-tables.yaml::1::aikeda
CREATE TABLE public.authentication (id BIGSERIAL NOT NULL, username VARCHAR(120) NOT NULL, password VARCHAR(200) NOT NULL, email_address VARCHAR(180) NOT NULL, enabled BOOLEAN DEFAULT FALSE NOT NULL, CONSTRAINT PK_AUTHENTICATION PRIMARY KEY (id, username));

INSERT INTO public.databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE) VALUES ('1', 'aikeda', '/Users/anthonyikeda/work/git_poc/macaroon-user-api/src/main/resources/changelog/01-changelog-tables.yaml', NOW(), 2, '7:4675569f80923eb09ec7c9d1c954a236', 'createTable', '', 'EXECUTED', NULL, NULL, '3.4.2');

-- Release Database Lock
UPDATE public.databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;
```



## Example

```bash

$ http --print=HhBb :8080/user "Authorization: macaroon MDAyOWxvY2F0aW9uIGh0dHA6Ly9sb2NhbGhvc3Q6ODA4My9sb2dpbgowMDIwaWRlbnRpZmllciBtYWMtbG9naW4tc2VydmVyCjAwMmZzaWduYXR1cmUgKAjJBYGL6bkUt0HZvYa8IkrXthSm7_Au9DG-uq90gqkK"
GET /user HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Authorization: macaroon MDAyOWxvY2F0aW9uIGh0dHA6Ly9sb2NhbGhvc3Q6ODA4My9sb2dpbgowMDIwaWRlbnRpZmllciBtYWMtbG9naW4tc2VydmVyCjAwMmZzaWduYXR1cmUgKAjJBYGL6bkUt0HZvYa8IkrXthSm7_Au9DG-uq90gqkK
Connection: keep-alive
Host: localhost:8080
User-Agent: HTTPie/0.9.9



HTTP/1.1 200 
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
Content-Type: application/json;charset=UTF-8
Date: Sun, 10 Jun 2018 20:14:10 GMT
Expires: 0
Pragma: no-cache
Transfer-Encoding: chunked
X-Content-Type-Options: nosniff
X-Frame-Options: DENY
X-XSS-Protection: 1; mode=block

[
    {
        "accountNonExpired": true,
        "accountNonLocked": true,
        "authorities": null,
        "credentialsNonExpired": true,
        "emailAddress": "user@home.com",
        "enabled": false,
        "password": null,
        "userId": 1,
        "username": null
    }
]

```

Bad Request:

```bash
$ http --print=HhBb :8080/user
  GET /user HTTP/1.1
  Accept: */*
  Accept-Encoding: gzip, deflate
  Connection: keep-alive
  Host: localhost:8080
  User-Agent: HTTPie/0.9.9
  
  
  
  HTTP/1.1 401 
  Cache-Control: no-cache, no-store, max-age=0, must-revalidate
  Content-Length: 0
  Date: Sun, 10 Jun 2018 20:27:37 GMT
  Expires: 0
  Pragma: no-cache
  X-Content-Type-Options: nosniff
  X-Frame-Options: DENY
  X-XSS-Protection: 1; mode=block

```

## TODO

<b>Implement Caveat comparison in the `MacaroonAspect`</b>

An annotation has been created (`@MacaroonCaveats`)to apply to methods and track which caveats are required for the method to execute:

```java
//annotation to define the caveats, AOP Before any method that implements the interface assert the caveats they expect.
@RequestMapping(path = "/user", method = RequestMethod.GET, produces = {"application/json"})
@MacaroonCaveats(caveats = { "name=ikeda", "time<{date}"})
public ResponseEntity<List<User>> getUsers() {
    log.debug("Listing alls users...");
    List<User> users = this.userService.getUsers().orElse(Collections.EMPTY_LIST);

    log.debug("Found {} users", users.size());

    return ResponseEntity.ok(users);
}

```

The Annotation currently just captures the caveats and comparing them to the incoming Macaroons has not yet been implemented.

The `MacaroonAspect` should use IOC to compare the Macaroon in the Security Context with the defined Caveats on the method and either allow or deny access to the method.

The `MacaroonCaveat` annotation is a method level annotation. I seem to be having problems accessing the `SecurityContext` in the `MacaroonCaveat`.

